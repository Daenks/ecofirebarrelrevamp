﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FireBarrel
{
    using System;
    using Eco.Core.Plugins.Interfaces;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Mods.TechTree;

    [Serialized]
    [RequireComponent(typeof(AirPollutionComponent))]
    [RequireComponent(typeof(OnOffComponent))]
    [RequireComponent(typeof(ChimneyComponent))]
    [RequireComponent(typeof(LiquidProducerComponent))]
    [RequireComponent(typeof(AttachmentComponent))]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(MinimapComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(CraftingComponent))]
    [RequireComponent(typeof(FuelSupplyComponent))]
    [RequireComponent(typeof(FuelConsumptionComponent))]
    [RequireComponent(typeof(HousingComponent))]
    [RequireComponent(typeof(SolidGroundComponent))]
    [RequireComponent(typeof(LiquidConverterComponent))]

    public partial class FireBarrelObject : WorldObject, IModKitPlugin
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Fire Barrel"); } }
        public virtual Type RepresentedItemType { get { return typeof(FireBarrelItem); } }

        public string GetStatus()
        {
            return String.Empty;
        }

        static FireBarrelObject()
        {
            AddOccupancyList(typeof(FireBarrelObject), new BlockOccupancy(new Vector3i(0, 0, 0), typeof(WorldObjectBlock), new Quaternion(0f, 0f, 0f, 1f)));
        }

        private static Type[] fuelTypeList = new Type[]
        {
            typeof(ArrowItem),
            typeof(BoardItem),
            typeof(CharcoalItem),
            typeof(CoalItem),
            typeof(LogItem),
            typeof(LumberItem),
        };

        [Serialized] private bool fireEnabled = false;
        [Serialized] private float garbageWeightFloat = 0f;

        protected override void Initialize()
        {
            this.GetComponent<MinimapComponent>().Initialize("Fire Barrel");
            this.GetComponent<FuelSupplyComponent>().Initialize(4, fuelTypeList);
            this.GetComponent<FuelConsumptionComponent>().Initialize(20f);
            this.GetComponent<AirPollutionComponent>().Initialize(0.1f);
            this.GetComponent<PropertyAuthComponent>().Initialize();
            BlockOccupancy thisBlock = this.Occupancy.Find(x => x.Name == "ChimneyOut");
            thisBlock.Offset = new Vector3i(0, 2, 0); //offset of chimney output
            thisBlock.BlockType = typeof(WorldObjectBlock);

            this.GetComponent<LiquidProducerComponent>().Setup(typeof(SmogItem), (int)(1 * 1000f), this.NamedOccupancyOffset("ChimneyOut"));
        }

        public override void Destroy()
        {
            base.Destroy();
        }

        public override void Tick()
        {
            base.Tick();

            if (CheckFireEnabled())
            { //check if there's fuel and garbage in the barrel, set animation state, set operating state
                garbageWeightFloat -= 2.0f;
            }
        }

        public override InteractResult OnActRight(InteractionContext context)
        {
            if (context.Parameters != null && context.Parameters.ContainsKey("TakeButton"))
            {
                TakeBarrel(context); //add fuel inventory to player, give them the barrel
                return InteractResult.Success;
            }
            else
            {
                ShowInfo(context); //show garbage weight
                return InteractResult.Success;
            }
        }

        public override InteractResult OnActLeft(InteractionContext context)
        {
            if (context.SelectedItem != null)
            {
                string currentItemName = context.SelectedItem.DisplayName;

                bool stackMode = CheckStackMode(context.Player.User.Inventory.Toolbar.SelectedStack.Weight); //if garbage barrel + selected stack weight is less than barrel max weight

                if ((garbageWeightFloat / 1000f) < 9.9) //this check is run here instead of AddTrash so the weight can go over the max weight by 1 item
                    AddTrash(context, currentItemName, stackMode); //add to garbageWeight, notify player they threw x item(s) in trash, remove item(s)
                else
                {
                    LocStringBuilder locStringBuilder = new LocStringBuilder();
                    locStringBuilder.Append("The barrel is full.");
                    context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                }
                return InteractResult.Success;
            }

            return InteractResult.NoOp;
        }

        private void AddTrash(InteractionContext context, string currentItemName, bool stackMode)
        {
            if (stackMode)
            {
                garbageWeightFloat += CheckGarbageWeight(context, true);

                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You threw " + context.Player.User.Inventory.Toolbar.SelectedStack.Quantity.ToString() + " " + currentItemName + " into the fire barrel.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                context.Player.User.Inventory.TryRemoveItems(context.Player.User.Inventory.Toolbar.SelectedStack);
            }
            else
            {
                garbageWeightFloat += CheckGarbageWeight(context, false);

                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You threw " + currentItemName + " into the fire barrel.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                context.Player.User.Inventory.TryRemoveItem(context.SelectedItem.Type);
            }
        }

        private float CheckGarbageWeight(InteractionContext context, bool stack)
        {
            float addGarbageWeight = 0.0f;

            if (stack)
                addGarbageWeight = context.Player.User.Inventory.Toolbar.SelectedStack.Weight;
            else
                addGarbageWeight = context.SelectedItem.Weight;

            if (addGarbageWeight == 0.0f)
                addGarbageWeight = 1000f;

            return addGarbageWeight;
        }

        private bool CheckStackMode(float selectedStackWeight)
        {
            bool stackMode = false;

            if (garbageWeightFloat / 1000f + selectedStackWeight / 1000f < 9.9)
                stackMode = true;
            else
                stackMode = false;

            return stackMode;
        }

        private void TakeBarrel(InteractionContext context)
        {
            if (garbageWeightFloat < 0.01f)
                AddItemsFromBarrel(context); //Make sure the items can be added
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You can not pick up the barrel with trash already in it.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }
        }

        private void AddItemsFromBarrel(InteractionContext context)
        {
            FuelSupplyComponent fuelSupply = this.GetComponent<FuelSupplyComponent>();

            bool addLog = false;
            bool addLumber = false;
            bool addArrow = false;
            bool addBoard = false;
            bool addCharcoal = false;
            bool addCoal = false;

            if (context.Player.User.Inventory.TryAddItems(typeof(LogItem), fuelSupply.Inventory.TotalNumberOfItems<LogItem>()))
            {
                fuelSupply.Inventory.RemoveItems<LogItem>(fuelSupply.Inventory.TotalNumberOfItems<LogItem>());
                addLog = true;
            }
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You do not have enough inventory space to pick up all of the logs.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }

            if (context.Player.User.Inventory.TryAddItems(typeof(LumberItem), fuelSupply.Inventory.TotalNumberOfItems<LumberItem>()))
            {
                fuelSupply.Inventory.RemoveItems<LumberItem>(fuelSupply.Inventory.TotalNumberOfItems<LumberItem>());
                addLumber = true;
            }
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You do not have enough inventory space to pick up all of the lumber.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }

            if (context.Player.User.Inventory.TryAddItems(typeof(ArrowItem), fuelSupply.Inventory.TotalNumberOfItems<ArrowItem>()))
            {
                fuelSupply.Inventory.RemoveItems<ArrowItem>(fuelSupply.Inventory.TotalNumberOfItems<ArrowItem>());
                addArrow = true;
            }
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You do not have enough inventory space to pick up all of the arrows.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }

            if (context.Player.User.Inventory.TryAddItems(typeof(BoardItem), fuelSupply.Inventory.TotalNumberOfItems<BoardItem>()))
            {
                fuelSupply.Inventory.RemoveItems<BoardItem>(fuelSupply.Inventory.TotalNumberOfItems<BoardItem>());
                addBoard = true;
            }
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You do not have enough inventory space to pick up all of the boards.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }

            if (context.Player.User.Inventory.TryAddItems(typeof(CharcoalItem), fuelSupply.Inventory.TotalNumberOfItems<CharcoalItem>()))
            {
                fuelSupply.Inventory.RemoveItems<CharcoalItem>(fuelSupply.Inventory.TotalNumberOfItems<CharcoalItem>());
                addCharcoal = true;
            }
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You do not have enough inventory space to pick up all of the charcoal.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }

            if (context.Player.User.Inventory.TryAddItems(typeof(CoalItem), fuelSupply.Inventory.TotalNumberOfItems<CoalItem>()))
            {
                fuelSupply.Inventory.RemoveItems<CoalItem>(fuelSupply.Inventory.TotalNumberOfItems<CoalItem>());
                addCoal = true;
            }
            else
            {
                LocStringBuilder locStringBuilder = new LocStringBuilder();
                locStringBuilder.Append("You do not have enough inventory space to pick up all of the coal.");
                context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
            }

            if (addLog && addLumber && addArrow && addBoard && addCharcoal && addCoal)
            {
                if (context.Player.User.Inventory.TryAddItem(typeof(FireBarrelItem)))
                {
                    this.Destroy();
                }
                else
                {
                    LocStringBuilder locStringBuilder = new LocStringBuilder();
                    locStringBuilder.Append("You do not have enough inventory space to pick up the fire barrel.");
                    context.Player.SendTemporaryMessage(locStringBuilder.ToLocString());
                }
            }
        }

        private void ShowInfo(InteractionContext context)
        {
            float garbagePercent = ((garbageWeightFloat / 1000f) / 10.0f) * 100;
            garbagePercent = (float)Math.Round(garbagePercent, 2);

            context.Player.OpenInfoPanel("Fire Barrel Info", (GetColor(garbagePercent) + "<size=50>" + garbagePercent.ToString() + "% full</size>"));
        }

        private bool CheckFireEnabled()
        {
            FuelSupplyComponent fuelSupplyComponent = this.GetComponent<FuelSupplyComponent>();
            float fuelSupply = fuelSupplyComponent.Energy;

            if (fuelSupply > 0 && (garbageWeightFloat) > 0)
            {
                fireEnabled = true;
            }
            else if (garbageWeightFloat < 0)
            {
                garbageWeightFloat = 0f; //lazy fix for negative values
                fireEnabled = false;
            }
            else
                fireEnabled = false;

            SetAnimatedState("FireBarrel", fireEnabled);
            this.operating = fireEnabled;
            return fireEnabled;
        }

        private string GetColor(float garbagePercent)
        {
            string colorString = "";

            if (garbagePercent < 25)
                colorString = "<color=#ffffff>";
            else if (garbagePercent >= 25 && garbagePercent < 50)
                colorString = "<color=#33cc33>";
            else if (garbagePercent >= 50 && garbagePercent < 75)
                colorString = "<color=#ffff00>";
            else if (garbagePercent >= 75 && garbagePercent < 100)
                colorString = "<color=#ff6600>";
            else if (garbagePercent >= 100)
                colorString = "<color=#ff0000>";

            return colorString;
        }
    }

    [Serialized]
    public partial class FireBarrelItem : WorldObjectItem<FireBarrelObject>
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Fire Barrel"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("A barrel you can burn trash items in."); } }

        static FireBarrelItem()
        {

        }

    }

    [RequiresSkill(typeof(SmeltingSkill), 0)]
    public partial class FireBarrelRecipe : Recipe
    {
        public FireBarrelRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<FireBarrelItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<IronIngotItem>(typeof(SmeltingSkill), 25, SmeltingSkill.MultiplicativeStrategy),
            };

            this.CraftMinutes = CreateCraftTimeValue(typeof(FireBarrelRecipe), Item.Get<FireBarrelItem>().UILink(), 5.0f, typeof(SmeltingSkill), typeof(SmeltingFocusedSpeedTalent), typeof(SmeltingParallelSpeedTalent));
            this.Initialize(DisplayName, typeof(FireBarrelRecipe));
            CraftingComponent.AddRecipe(typeof(AnvilObject), this);
        }
    }
}
